const coin = {
    state: 0,
    flip: function() {
       this.state = Math.floor(Math.random() * 2)
    },
    toString: function() {
        let output = "Heads"
        if(coin.state === 1){
            output = "Tails"
        }
        return output
    },
    toHTML: function() {
        const div = document.getElementById("body")
        const image = document.createElement('img')
        if(this.state === 0){
            image.src = "./head.jpeg"
        }else {
            image.src = "./tail.jpeg"
        }
        div.appendChild(image)

    }
 }
 
 function display20Flips() {
    const results = [];
    for(i=0; i< 20; i++){
        coin.flip()
        results.push(coin.toString())
        const body = document.getElementById("body")
        const div = document.createElement('div')
        div.innerText = coin.toString()
        body.appendChild(div)
    }
    return results

 }
 
 function display20Images() {
    const results = [];
    for(i=0; i< 20; i++){
        coin.flip()
        coin.toHTML()
        results.push(coin.toString())
    }
    return results
 }
 display20Flips()
 display20Images()